///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 03d - CatPower - EE 205 - Spr 2022
///
/// Usage:  catPower fromValue fromUnit toUnit
///    fromValue: A number that we want to convert
///    fromUnit:  The energy unit fromValue is in
///    toUnit:  The energy unit to convert to
///
/// Result:
///   Print out the energy unit conversion
///
/// Example:
///   $ ./catPower 3.45e20 e j
///   3.45E+20 e is 55.2751 j
///
/// Compilation:
///   $ g++ -o catPower catPower.cpp
///
/// @file catPower.cpp
/// @version 1.0
///
/// @see https://en.wikipedia.org/wiki/Units_of_energy
/// @see https://en.wikipedia.org/wiki/List_of_unusual_units_of_measurement#Energy
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date   29_JAN_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

#include "unit_conversion.h"

int main(int argc, char *argv[])
{
   // User input forward declarations
   double fromValue;
   char fromUnit;
   char toUnit;

   if (argc <= 1) // program was involked without inputs
   {
      print_instructions();
   }
   else // program was involked with inputs
   {
      fromValue = atof(argv[1]);
      fromUnit = argv[2][0];
      toUnit = argv[3][0];
      double toValue = convertFromJoules(convertToJoules(fromValue, fromUnit), toUnit);
      printf("%lG %s is %.4lG %s\n", fromValue, displayUnit(fromValue, fromUnit), toValue, displayUnit(toValue, toUnit));
   }

   return 0;
}