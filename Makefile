###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 03d - catPower - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Build and test a program that converts a user specified input
### from one unit to another
###
### Usage:  catPower fromValue fromUnit toUnit\n");
### 	fromValue: A number that we want to convert\n");
### 	fromUnit:  The energy unit fromValue is in\n");
### 	toUnit:  The energy unit to convert to\n");
###
### @author Caleb Mueller <mc61@hawaii.edu>
### @date   29_JAN_2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

all: 
	g++ -o catPower catPower.cpp unit_conversion.cpp

run:
	g++ -o catPower catPower.cpp unit_conversion.cpp && ./catPower

clean: 
	rm catPower