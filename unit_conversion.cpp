///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 03d - CatPower - EE 205 - Spr 2022
///
/// Usage:  catPower fromValue fromUnit toUnit
///    fromValue: A number that we want to convert
///    fromUnit:  The energy unit fromValue is in
///    toUnit:  The energy unit to convert to
///
/// Result:
///   Print out the energy unit conversion
///
/// Example:
///   $ ./catPower 3.45e20 e j
///   3.45E+20 e is 55.2751 j
///
/// Compilation:
///   $ g++ -o catPower catPower.cpp
///
/// @file unit_conversion.cpp
/// @version 1.0
///
/// @see https://en.wikipedia.org/wiki/Units_of_energy
/// @see https://en.wikipedia.org/wiki/List_of_unusual_units_of_measurement#Energy
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date   29_JAN_2022
///////////////////////////////////////////////////////////////////////////////

#include "unit_conversion.h"

void print_instructions()
{
    printf("Energy converter\n");
    printf("Usage:  catPower fromValue fromUnit toUnit\n");
    printf("   fromValue: A number that we want to convert\n");
    printf("   fromUnit:  The energy unit fromValue is in\n");
    printf("   toUnit:  The energy unit to convert to\n");
    printf("\n");
    printf("This program converts energy from one energy unit to another.\n");
    printf("The units it can convert are: \n");
    printf("   j = Joule\n");
    printf("   e = eV = electronVolt\n");
    printf("   m = MT = megaton of TNT\n");
    printf("   g = GGE = gasoline gallon equivalent\n");
    printf("   f = foe = the amount of energy produced by a supernova\n");
    printf("   c = catPower = like horsePower, but for cats\n");
    printf("\n");
    printf("To convert from one energy unit to another, enter a number \n");
    printf("it's unit and then a unit to convert it to.  For example, to\n");
    printf("convert 100 Joules to GGE, you'd enter:  $ catPower 100 j g\n");
    printf("\n");
}

double convertToJoules(const double initialValue, const char fromUnit)
{
    double valueInJoules = 0;
    switch (fromUnit)
    {
    case 'j':
    {
        // Conversion of Joules to Joules
        valueInJoules = initialValue;
        break;
    }
    case 'e':
    {
        // Conversion of Electron Volts to Joules
        valueInJoules = initialValue * JOULES_PER_ELECTRON_VOLT;
        break;
    }
    case 'm':
    {
        // Conversion of Mega Tons to Joules
        valueInJoules = initialValue * JOULES_PER_MEGATON;
        break;
    }
    case 'g':
    {
        // Conversion of Gallons of Gasoline to Joules
        valueInJoules = initialValue * JOULES_PER_GALLONSGAS;
        break;
    }
    case 'f':
    {
        // Conversion of Foes to Joules
        valueInJoules = initialValue * JOULES_PER_FOE;
        break;
    }
    case 'c':
    {
        // Conversion of Cat Power to Joules
        valueInJoules = 0.0; // Cats do no work
        break;
    }
    default:
        break;
    }
    return valueInJoules;
}

double convertFromJoules(const double valueInJoules, const char toUnit)
{
    double outputValue;
    switch (toUnit)
    {
    case 'j':
    {
        // Conversion of Joules to Joules
        outputValue = valueInJoules;
        break;
    }
    case 'e':
    {
        // Conversion of Joules to Electron Volts
        outputValue = valueInJoules / JOULES_PER_ELECTRON_VOLT;
        break;
    }
    case 'm':
    {
        // Conversion of Joules to Mega Tons
        outputValue = valueInJoules / JOULES_PER_MEGATON;
        break;
    }
    case 'g':
    {
        // Conversion of Joules to Gallons of Gasoline
        outputValue = valueInJoules / JOULES_PER_GALLONSGAS;
        break;
    }
    case 'f':
    {
        // Conversion of Joules to Foes
        outputValue = valueInJoules / JOULES_PER_FOE;
        break;
    }
    case 'c':
    {
        // Conversion of Joules to Cat Power
        outputValue = 0.0; // Cats do no work
        break;
    }
    default:
        break;
    }
    return outputValue;
}

const char *displayUnit(const double value, const char unitSpecifier)
{
    bool isPlural = false;
    if (value > 1.0 || value < -1)
    {
        isPlural = true;
    }

    switch (unitSpecifier)
    {
    case 'j':
    {
        if (isPlural)
        {
            return "Joules";
        }
        else
        {
            return "Joule";
        }
    }
    case 'e':
    {
        if (isPlural)
        {
            return "Electron-Volts";
        }
        else
        {
            return "Electron-Volt";
        }
    }
    case 'm':
    {
        if (isPlural)
        {
            return "Mega-Tons-of-TNT";
        }
        else
        {
            return "Mega-Ton-of-TNT";
        }
    }
    case 'g':
    {
        if (isPlural)
        {
            return "Gas-Gallon-Equivalents";
        }
        else
        {
            return "Gas-Gallon-Equivalent";
        }
    }
    case 'f':
    {
        if (isPlural)
        {
            return "Foes";
        }
        else
        {
            return "Foe";
        }
    }
    case 'c':
    {
        return "Cat-Power";
    }
    default:
        break;
    }
    return NULL;
}