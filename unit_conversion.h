///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 03d - CatPower - EE 205 - Spr 2022
///
/// Usage:  catPower fromValue fromUnit toUnit
///    fromValue: A number that we want to convert
///    fromUnit:  The energy unit fromValue is in
///    toUnit:  The energy unit to convert to
///
/// Result:
///   Print out the energy unit conversion
///
/// Example:
///   $ ./catPower 3.45e20 e j
///   3.45E+20 e is 55.2751 j
///
/// Compilation:
///   $ g++ -o catPower catPower.cpp
///
/// @file unit_conversion.h
/// @version 1.0
///
/// @see https://en.wikipedia.org/wiki/Units_of_energy
/// @see https://en.wikipedia.org/wiki/List_of_unusual_units_of_measurement#Energy
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date   29_JAN_2022
///////////////////////////////////////////////////////////////////////////////

#ifndef UNIT_CONVERSION_H
#define UNIT_CONVERSION_H

#include <stdio.h>
#include <stdbool.h>

// Conversion Factors
const double JOULES_PER_ELECTRON_VOLT = 1.6022e-19;
const double JOULES_PER_MEGATON = 4.180e15;
const double JOULES_PER_GALLONSGAS = 1.213e8;
const double JOULES_PER_FOE = 1e44;

void print_instructions();
double convertToJoules(double numToConvert, char convertUnitFrom);
double convertFromJoules(double valueInJoules, char toUnit);
const char *displayUnit(const double value, const char unitSpecifier);

#endif